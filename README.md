﻿BINDER-MK53 README
===========
This LabVIEW project "CS++.lvproj" is used to control and reead out the process values from BINDER Mk53 climate chamber.

Currently used development SW is LabVIEW 2015

Related documents and information
=================================
- README.txt
- Release_Notes.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: A.Lzmanets@gsi.de
- Download, bug reports... : https://git.gsi.de/.git
- Documentation:
  - Refer to Documantation Folder 


Getting started:
=================================
- Clone this repository, if not alread done.
- Switch to the desired branch.

Known issues:
=============


Author: A.Lzmanets@gsi.de

Copyright 2018 A.Lymanets@gsi.de

GSI Helmholtzzentrum für Schwerionenforschung GmbH
CBM, Planckstr. 1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.