Release Notes for the Binder-MK53 Project - 11-Oct-2018
=================================================
Control and reead out the process values from BINDER Mk53 climate chamber.

Published under EUPL v1.1.

Copyright 2018 A.Lymanets@gsi.de

GSI Helmholtzzentrum f�r Schwerionenforschung GmbH
CBM, Planckstr. 1, 64291 Darmstadt, Germany


Version 0.0.0.0
============
The project was just started. There is the master branch with license only.
